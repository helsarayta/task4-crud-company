package com.heydiez.companycrud.service;

public class CompanyNotFoundException extends Throwable {
    public CompanyNotFoundException(String s) {
        super(s);
    }
}
