package com.heydiez.companycrud.repository;

import com.heydiez.companycrud.entity.Company;
import org.springframework.data.repository.CrudRepository;

public interface CompanyRepository extends CrudRepository<Company, Integer> {
    public Long countById(Integer id);
}
