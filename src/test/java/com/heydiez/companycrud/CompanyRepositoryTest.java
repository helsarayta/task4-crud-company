package com.heydiez.companycrud;

import com.heydiez.companycrud.entity.Company;
import com.heydiez.companycrud.repository.CompanyRepository;
import org.assertj.core.api.Assertions;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.jdbc.AutoConfigureTestDatabase;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.springframework.test.annotation.Rollback;

@DataJpaTest
@AutoConfigureTestDatabase(replace = AutoConfigureTestDatabase.Replace.NONE)
@Rollback(value = false)
public class CompanyRepositoryTest {
    @Autowired
    private CompanyRepository repository;

    @Test
    public void testAdd() {
        Company company = new Company();
        company.setId(3);
        company.setNama("HAFYA");
        company.setAlamat("JAKARTA");

        Company companySave = repository.save(company);

        Assertions.assertThat(companySave).isNotNull();
        Assertions.assertThat(companySave.getId()).isGreaterThan(0);

    }
}
