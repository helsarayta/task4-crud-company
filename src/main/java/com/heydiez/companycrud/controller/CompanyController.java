package com.heydiez.companycrud.controller;

import com.heydiez.companycrud.entity.Company;
import com.heydiez.companycrud.service.CompanyNotFoundException;
import com.heydiez.companycrud.service.CompanyService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import java.util.List;

@Controller
public class CompanyController {
    @Autowired
    private CompanyService companyService;

    @GetMapping("")
    public String showHomePage() {
        return "homepage";
    }

    @GetMapping("/company")
    public String showCompanyList(Model model) {
        List<Company> companyList = companyService.listAll();
        model.addAttribute("companyList", companyList);
        return "company";
    }

    @GetMapping("company/new")
    public String addNewCompany(Model model) {
        model.addAttribute("company", new Company());
        return "company_form";
    }

    @PostMapping("company/save")
    public String saveNewCompany(Company company, RedirectAttributes attributes) {
        companyService.save(company);
        attributes.addFlashAttribute("message", "New company has been save succesfully");
        return "redirect:/company";
    }

    @GetMapping("/company/edit/{id}")
    public String showEditForm(@PathVariable("id") Integer id, Model model, RedirectAttributes attributes) {
        try {
            Company company = companyService.get(id);
            model.addAttribute("company", company);
            model.addAttribute("pageTitle", "Edit Employee ID : " + id);
            return "company_form";
        } catch (CompanyNotFoundException e) {
            attributes.addFlashAttribute("message", e.getMessage());
            return "redirect:/company";
        }
    }

    @GetMapping("/company/delete/{id}")
    public String deleteCompany(@PathVariable("id") Integer id, RedirectAttributes attributes) {
        try {
            companyService.delete(id);
            attributes.addFlashAttribute("message", "Company ID " + id + " has been deleted");
        } catch (CompanyNotFoundException e) {
            attributes.addFlashAttribute("message", e.getMessage());
        }
        return "redirect:/company";
    }
}
