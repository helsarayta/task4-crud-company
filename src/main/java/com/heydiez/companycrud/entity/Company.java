package com.heydiez.companycrud.entity;

import lombok.Data;

import javax.persistence.*;

@Data
@Entity
@Table(name = "company")
public class Company {
    @Id
    private Integer id;

    @Column(nullable = false, length = 50)
    private String nama;

    @Column(nullable = false, length = 50)
    private String alamat;
}
