package com.heydiez.companycrud.service;

import com.heydiez.companycrud.entity.Company;
import com.heydiez.companycrud.repository.CompanyRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;

@Service
public class CompanyService {
    @Autowired
    private CompanyRepository companyRepository;

    public List<Company> listAll() {
        return (List<Company>) companyRepository.findAll();
    }

    public void save(Company company) {
        companyRepository.save(company);
    }

    public Company get(Integer id) throws CompanyNotFoundException {
        Optional<Company> companyOptional = companyRepository.findById(id);
        if (companyOptional.isPresent()) {
            return companyOptional.get();
        }
        throw new CompanyNotFoundException("Could not find any Company with ID " + id);
    }

    public void delete(Integer id) throws CompanyNotFoundException {
        Long count = companyRepository.countById(id);
        if (count == null || count == 0) {
            throw new CompanyNotFoundException("Could not find any Company with ID " + id);

        }
        companyRepository.deleteById(id);
    }
}
