package com.heydiez.companycrud;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class CompanycrudApplication {

	public static void main(String[] args) {
		SpringApplication.run(CompanycrudApplication.class, args);
	}

}
